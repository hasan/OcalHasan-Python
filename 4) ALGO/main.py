# ALGO - PROGRAMME PRINCIPAL

# Vous pouvez importer et tester vos fonctions ici
# Assurez-vous d'ajuster les valeurs d'exemple en conséquence.

# Exemple : Vérifier si un nombre est pair ou impair
def est_pair(nombre):
    if nombre % 2 == 0:
        return True
    else:
        return False

# Exemple d'utilisation
num_test = 7
if est_pair(num_test):
    print(f"{num_test} est un nombre pair.")
else:
    print(f"{num_test} est un nombre impair.")