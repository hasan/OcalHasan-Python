# ALGO - NOMBRE DIVISIBLE OU PAS?

# Programme pour vérifier si un nombre est divisible par 3 et 13

def divisible_par_3_et_13(nombre):
    if nombre % 3 == 0 and nombre % 13 == 0:
        return True
    else:
        return False

# Exemple d'utilisation
num = 39
if divisible_par_3_et_13(num):
    print(f"{num} est divisible par 3 et 13.")
else:
    print(f"{num} n'est pas divisible par 3 et 13.")