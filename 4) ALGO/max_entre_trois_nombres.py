# ALGO - NOMBRE LE PLUS GRAND

# Programme pour trouver le maximum entre trois nombres

def trouver_maximum(a, b, c):
    if a >= b and a >= c:
        return a
    elif b >= a and b >= c:
        return b
    else:
        return c

# Exemple d'utilisation
num1 = 10
num2 = 25
num3 = 15
maximum = trouver_maximum(num1, num2, num3)
print(f"Le maximum entre {num1}, {num2} et {num3} est : {maximum}")