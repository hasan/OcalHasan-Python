# note.py

class Note:
    def __init__(self):
        self.notes_dict = {}

    def ajouter_note(self, cle, valeur):
        self.notes_dict[cle] = valeur

    def afficher_note(self, cle):
        try:
            print(self.notes_dict[cle])
        except KeyError:
            print(f"La clé '{cle}' n'existe pas.")

# Exemple d'utilisation
# note_instance = Note()
# note_instance.ajouter_note("1", "Note 1")
# note_instance.ajouter_note("2", "Note 2")
# note_instance.afficher_note("1")
# note_instance.afficher_note("3")