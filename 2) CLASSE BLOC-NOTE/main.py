# main.py
from note import Note

def main():
    bloc_note = Note()

    # Ajout de quelques notes pour l'exemple
    bloc_note.ajouter_note("1", "Note 1")
    bloc_note.ajouter_note("2", "Note 2")
    bloc_note.ajouter_note("3", "Note 3")

    # Affichage des notes avec gestion d'erreur
    try:
        cle_demandee = input("Entrez la clé de la note à afficher : ")
        bloc_note.afficher_note(cle_demandee)
    except KeyboardInterrupt:
        print("\nProgramme interrompu par l'utilisateur.")
    except Exception as e:
        print(f"Une erreur s'est produite : {e}")

if __name__ == "__main__":
    main()