# joueur.py

class Joueur:
    def __init__(self, nom, vie, points):
        self.nom = nom
        self.vie = vie
        self.points = points

    # Getters
    def get_nom(self):
        return self.nom

    def get_vie(self):
        return self.vie

    def get_points(self):
        return self.points

    # Setters
    def set_nom(self, nom):
        self.nom = nom

    def set_vie(self, vie):
        self.vie = vie

    def set_points(self, points):
        self.points = points

    # Méthodes supplémentaires
    def ajouter_points(self, nombre_points):
        self.points += nombre_points

    def tuer_joueur(self):
        self.vie = 0

    def ressusciter_joueur(self, vie_initiale):
        self.vie = vie_initiale

    def descrire_joueur(self):
        return f"{self.nom} a {self.vie} points de vie et un score de {self.points}."