# main.py
from joueur import Joueur

# Création d'une instance de Joueur
joueur1 = Joueur("NomDuJoueur", 100, 0)

# Utilisation des méthodes et affichage des attributs
joueur1.ajouter_points(50)
joueur1.tuer_joueur()
joueur1.ressusciter_joueur(100)

# Affichage de la description du joueur
print(joueur1.descrire_joueur())