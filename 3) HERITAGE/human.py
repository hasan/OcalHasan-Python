# human.py
from life import Life

class Human(Life):
    def __init__(self, name, age, gender):
        super().__init__(name, age)
        self.gender = gender

    def speak(self):
        print("Hello, I am a human!")