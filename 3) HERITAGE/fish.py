# fish.py
from life import Life

class Fish(Life):
    def __init__(self, name, age, type):
        super().__init__(name, age)
        self.type = type

    def swim(self):
        print("The fish is swimming.")