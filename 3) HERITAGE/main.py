# main.py
from life import Life
from human import Human
from fish import Fish

# Create instances
human_instance = Human("Hasan", 31, "Male")
fish_instance = Fish("Nemo", 2, "Clownfish")

# Test methods
human_instance.display_info()
human_instance.speak()

fish_instance.display_info()
fish_instance.swim()